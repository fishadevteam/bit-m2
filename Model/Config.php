<?php

namespace Fisha\Bit\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Gateway\Config\Config as ConfigExtend;

/**
 * Class Config
 * @package Fisha\Bit\Model
 */
class Config extends ConfigExtend
{
    const BIT_API_TOKEN_URL = '/bank/auth/clients/token';
    const BIT_API_TOKEN_PATH = 'payment/bit/bit_token';
    const BIT_API_TOKEN_EXPIRE_PATH = 'payment/bit/bit_token_expire';
    const BIT_API_APIM_HOST = 'payment/bit/bit_apim_host';
    const BIT_API_SUBSCRIPTION_KEY = 'payment/bit/bit_subscription_key';
    const BIT_API_BITCOM_APP_ID = 'payment/bit/bit_bitcom_appid';
    const BIT_API_BITCOM_SECRET = 'payment/bit/bit_bitcom_secret';
    const BIT_API_BITCOM_FRANCHISING_ID = 'payment/bit/bit_franchising_id';
    const CERT_DIR = 'payment/bit/bit_cert_dir';
    const PK_FILE = 'payment/bit/bit_pk_file';
    const CERT_PATH = 'payment/bit/bit_cert_path';
    const CERT_FILE_NAME = 'payment/bit/bit_cert_file_name';
    const CERT_PASSWD = 'payment/bit/bit_cert_passwd';
    const TRANSACTION_TYPE = 'payment/bit/transaction_type';
    const CURRENCY_COD_TYPE = 1;
    const DEBIT_METHOD_CODE = 2;


    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param null $methodCode
     * @param string $pathPattern
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        $methodCode = null,
        $pathPattern = ConfigExtend::DEFAULT_PATH_PATTERN
    ) {
        $this->scopeConfig = $scopeConfig;
        ConfigExtend::__construct($scopeConfig, $methodCode, $pathPattern);
    }

    /**
     * @return string|null
     */
    public function getApimHost()
    {
        return $this->scopeConfig->getValue(
            self::BIT_API_APIM_HOST
        );
    }

    /**
     * @return string|null
     */
    public function getSubscriptionKey()
    {
        return $this->scopeConfig->getValue(
            self::BIT_API_SUBSCRIPTION_KEY
        );
    }

    /**
     * @return string|null
     */
    public function getBitcomAppId()
    {
        return $this->scopeConfig->getValue(
            self::BIT_API_BITCOM_APP_ID
        );
    }

    /**
     * @return string|null
     */
    public function getBitcomSecret()
    {
        return $this->scopeConfig->getValue(
            self::BIT_API_BITCOM_SECRET
        );
    }

    /**
     * @return mixed
     */
    public function getBitcomFranchisingId()
    {
        return $this->scopeConfig->getValue(
            self::BIT_API_BITCOM_FRANCHISING_ID
        );
    }

    /**
     * Get cert_passwd
     * @return string|null
     */
    public function getCertPasswd()
    {
        return $this->scopeConfig->getValue(
            self::CERT_PASSWD
        );
    }

    /**
     * Get cert_file_name
     * @return string|null
     */
    public function getCertFileName()
    {
        return $this->scopeConfig->getValue(
            self::CERT_FILE_NAME
        );
    }

    /**
     * Get cert_path
     * @return string|null
     */
    public function getCertPath()
    {
        return $this->scopeConfig->getValue(
            self::CERT_PATH
        );
    }

    /**
     * Get pk_file
     * @return string|null
     */
    public function getPkFile()
    {
        return $this->scopeConfig->getValue(
            self::PK_FILE
        );
    }

    /**
     * Get cert_dir
     * @return string|null
     */
    public function getCertDir()
    {
        return $this->scopeConfig->getValue(
            self::CERT_DIR
        );
    }

    /**
     * Get transaction_type
     * @return string|null
     */
    public function getTransactionType()
    {
        return $this->scopeConfig->getValue(
            self::TRANSACTION_TYPE
        );
    }
}
