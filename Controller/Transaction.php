<?php

namespace Fisha\Bit\Controller;

use Fisha\Bit\Logger\DebugLogger;
use Fisha\Bit\Logger\ErrorLogger;
use Fisha\Bit\Model\Payment\Bit;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Class Transaction
 * @package Fisha\Bit\Controller
 */
abstract class Transaction extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var DebugLogger
     */
    protected $debugLogger;

    /**
     * @var ErrorLogger
     */
    protected $errorLogger;

    /**
     * @var Bit
     */
    protected $bit;

    /**
     * Transaction constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Bit $bit
     * @param DebugLogger $debugLogger
     * @param ErrorLogger $errorLogger
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Bit $bit,
        DebugLogger $debugLogger,
        ErrorLogger $errorLogger
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->debugLogger = $debugLogger;
        $this->errorLogger = $errorLogger;
        $this->bit = $bit;

        parent::__construct($context);
    }
}
