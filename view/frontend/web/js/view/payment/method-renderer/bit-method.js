define([
    'Magento_Checkout/js/view/payment/default',
    'ko',
    'jquery',
    'mage/url',
    'Magento_Checkout/js/action/select-payment-method',
    'Magento_Checkout/js/action/set-payment-information',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/quote',
    'Magento_Ui/js/model/messages',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/model/full-screen-loader'
], function (
    Component,
    ko,
    $,
    mageUrl,
    selectPaymentMethodAction,
    setPaymentInformationAction,
    checkoutData,
    quote,
    Messages,
    customerData) {

    'use strict';

    var checkoutConfig = window.checkoutConfig,
        agreementsConfig = checkoutConfig ? checkoutConfig.checkoutAgreements : {};
    return Component.extend({
        defaults: {
            template: 'Fisha_Bit/payment/bit',
            paymentReady: false,
            agreementCheckboxSelector: '.payment-method.bit .checkout-agreements-block input'
        },
        buttonIsInitialized: false,

        agreementIsChecked: function () {
            var isChecked = true;
            if (agreementsConfig.isEnabled && !$(this.agreementCheckboxSelector).is(":checked")) {
                isChecked = false;
            }
            return isChecked;
        },

        /**agreementIsChecked
         * @return {exports}
         */
        initialize: function () {
            this._super()
                .observe(['paymentReady','agreementIsChecked']);
            var self = this;
            if (agreementsConfig.isEnabled) {
                $(document).on('change', this.agreementCheckboxSelector, function () {
                    self.agreementIsChecked();
                    self.initTransaction();
                }.bind(this));
            }

            this.checkQuote();

            return this;
        },

        checkQuote: function () {
            let paymentId = window.checkoutConfig.quoteData['bit_payment_initiation_id'];
            console.log('bit_payment_initiation_id: ' + paymentId);
            if (paymentId !== undefined && paymentId !== null) {
                let email = window.checkoutConfig.quoteData.customer_email;
                customerData.reload(['customer']);
                $("form[data-role=email-with-possible-login] input[name=username]").val(email).change();

                this.processPaymentStatus(paymentId, 1000);
            }

            return false;
        },

        /**
         * @return {*}
         */
        isPaymentReady: function () {
            return this.paymentReady();
        },

        initTransaction: function () {
            $('body').trigger('processStart');
            var self = this;
            $.ajax({
                url: mageUrl.build('bit/transaction/init'),
                data: {},
                type: "POST"
            }).done(function (response) {
                console.log('initTransaction response:');
                console.log(response);
                let objJSON = JSON.parse(response);
                if (self.validateResponse(objJSON)) {
                    self.transactionSerialId = objJSON.transactionSerialId;
                    self.paymentInitiationId = objJSON.paymentInitiationId;
                    self.applicationSchemeAndroid = objJSON.applicationSchemeAndroid;
                    self.applicationSchemeIos = objJSON.applicationSchemeIos;
                }

                self.initButton();
            });
        },

        initButton: function () {
            function initDesktopButton() {
                if (self.agreementIsChecked()) {
                    BitPayment.Buttons({
                        onCreate: function (openBitPaymentPage) {
                            var transaction = {
                                transactionSerialId: self.transactionSerialId,
                                paymentInitiationId: self.paymentInitiationId
                            };
                            openBitPaymentPage(transaction);
                        },
                        onApproved: function (details) {
                            self.processPaymentStatus(details.paymentInitiationId, 1000);
                        },
                        onCancel: function (data) {
                            console.log('cancel');
                            self.initTransaction();
                        },
                        onTimeout: function (details) {
                            console.log('timeout');
                            self.initTransaction();
                        }
                    }).render('#bitcom-button-container');
                }
            }

            function initMobileButton(os) {
                if (self.agreementIsChecked()) {
                    let href;
                    if (os === 'android') {
                        href = self.applicationSchemeAndroid;
                    } else if (os === 'ios') {
                        href = self.applicationSchemeIos;
                    } else {
                        console.log('Unhandled device environment');
                    }
                    console.log('href: ' + href);
                    $('#bit-payment-button-mobile').attr("href", self.buildReturnUrl(href)).show();
                    $('#bit-payment-button-mobile').on('click', function () {
                        $.ajax({
                            url: mageUrl.build('bit/transaction/updatequote'),
                            data: {paymentInitiationId: self.paymentInitiationId},
                            type: "POST"
                        }).done(function () {
                            console.log('Quote is updated');
                        });
                    });
                }
            }


            var self = this;

            if (self.buttonIsInitialized === true) {
                $('#bitcom-button-container iframe').remove();
            }

            $.ajax({
                url: mageUrl.build('bit/transaction/getenvironment'),
                data: {},
                type: "POST"
            }).done(function (environment) {
                console.log(environment);
                if (environment === 'desktop') {
                    initDesktopButton();
                } else {
                    initMobileButton(environment);
                }

                self.buttonIsInitialized = true;
                $('body').trigger('processStop')
            });

        },

        buildReturnUrl: function (url) {
            return url +
                '%26return_scheme%3D' +
                encodeURIComponent(encodeURIComponent(mageUrl.build('bit/transaction/returnurl/quoteid/' + window.checkoutConfig.quoteItemData[0]['quote_id'])));
        },

        processPaymentStatus: function (paymentInitiationId, delay) {
            var self = this;
            function getPaymentStatus() {
                $.ajax({
                    url: mageUrl.build('bit/transaction/getpaymentstatus'),
                    data: {paymentInitiationId: paymentInitiationId},
                    type: "POST"
                }).done(function (response) {
                    let objJSON = JSON.parse(response);
                    if (self.validateResponse(objJSON) && objJSON.hasOwnProperty('requestStatusCode')) {
                        console.log('Status: ' + objJSON.requestStatusCode);

                        if (objJSON.requestStatusCode === 9) {
                            self.captureTransaction(response);
                        } else if (objJSON.requestStatusCode === 11) {
                            console.log('Place order');
                            self.isPlaceOrderActionAllowed(true);
                            self.placeOrder();
                        } else {
                            setTimeout(getPaymentStatus, delay);
                        }
                    } else {
                        console.log('error in payment status/response:');
                        console.log(response);
                    }
                });
            }


            $('body').trigger('processStart');
            getPaymentStatus();
        },

        captureTransaction: function(data) {
            var self = this;
            $.ajax({
                url: mageUrl.build('bit/transaction/capture'),
                data: {data: data},
                type: "POST"
            }).done(function (response) {
                console.log('capture transaction response:');
                console.log(response);
                let objJSON = JSON.parse(response);
                if (self.validateResponse(objJSON)) {
                    console.log('Transaction captured');
                    if (objJSON.hasOwnProperty('placeOrder') && objJSON.placeOrder === 1) {
                        self.isPlaceOrderActionAllowed(true);
                        self.placeOrder();
                    } else {
                        self.processPaymentStatus(objJSON.paymentInitiationId, 1000);
                    }
                }
            });
        },

        validateResponse: function (response) {
            if (response.hasOwnProperty('incidentId')) {
                console.log('incidentId: ', response.incidentId);
                return false;
            }
            return true;
        },

        /**
         * Get payment method data
         */
        getData: function () {
            return {
                'method': this.item.method,
                'po_number': null,
                'additional_data': null,
                'extension_attributes': {agreement_ids: ["1"]}
            };
        },

        /**
         * @param forceLoad
         * @returns {boolean}
         */
        selectPaymentMethod: function (forceLoad) {
            selectPaymentMethodAction(this.getData());
            checkoutData.setSelectedPaymentMethod(this.item.method);
            this.messageContainer = new Messages();
            setPaymentInformationAction(this.messageContainer, this.getData());
            this.initTransaction();
            return true;
        },
    });
});
