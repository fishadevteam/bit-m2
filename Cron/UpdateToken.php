<?php
namespace Fisha\Bit\Cron;

use Psr\Log\LoggerInterface;
use Fisha\Bit\Model\UpdateToken as UpdateTokenModel;


class UpdateToken {
    protected $logger;

    /**
     * @var UpdateTokenModel
     */
    private $updateTokenModel;

    /**
     * UpdateToken constructor.
     * @param LoggerInterface $logger
     * @param UpdateTokenModel $updateTokenModel
     */
    public function __construct(
        LoggerInterface $logger,
        UpdateTokenModel $updateTokenModel
    ) {
        $this->logger = $logger;
        $this->updateTokenModel = $updateTokenModel;
    }

    /**
     * Write to system.log
     *
     * @return void
     */
    public function execute() {
        $this->logger->debug('Updated bit Token');
        $this->updateTokenModel->execute();
    }
}