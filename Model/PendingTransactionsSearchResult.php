<?php

namespace Fisha\Bit\Model;

use Magento\Framework\Api\SearchResults;
use Fisha\Bit\Api\Data\PendingTransactionsSearchResultInterface;

/**
 * Class PendingTransactionsSearchResult
 * @package Fisha\Bit\Model
 */
class PendingTransactionsSearchResult extends SearchResults implements PendingTransactionsSearchResultInterface
{

}
