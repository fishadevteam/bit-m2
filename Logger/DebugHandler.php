<?php
namespace Fisha\Bit\Logger;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;

/**
 * Class DebugHandler
 * @package Fisha\Bit\Logger
 */
class DebugHandler extends Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::DEBUG;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/bit/debug.log';
}
