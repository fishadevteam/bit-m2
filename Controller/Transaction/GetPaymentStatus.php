<?php

namespace Fisha\Bit\Controller\Transaction;

use Fisha\Bit\Controller\Transaction;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class GetPaymentStatus
 * @package Fisha\Bit\Controller\Transaction
 */
class GetPaymentStatus extends Transaction
{
    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            if (isset($_REQUEST['paymentInitiationId'])) {
                $response = $this->bit->getTransactionStatus($_REQUEST['paymentInitiationId']);
                $result->setData($response);
            }

            return $result;
        }
    }
}
