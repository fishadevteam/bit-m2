<?php
declare(strict_types=1);

namespace Fisha\Bit\Console;
use Fisha\Bit\Model\Request\InitFactory;
use Fisha\Bit\Model\Service;
use Fisha\Bit\Model\UpdateToken as UpdateTokenModel;
use Magento\Framework\App\Config\Storage\WriterInterface as WriteConfig;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateToken extends Command
{
    public const CONSOLE_COMMAND_NAME = 'bit:update_token';

    /**
     * @var InitFactory
     */
    private $initFactory;
    /**
     * @var Service
     */
    private $service;

    /**
     * @var WriteConfig
     */
    private $writeConfig;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var UpdateTokenModel
     */
    private $updateTokenModel;

    /**
     * UpdateToken constructor.
     * @param Service $service
     * @param InitFactory $initFactory
     * @param WriteConfig $writeConfig
     * @param TimezoneInterface $timezone
     * @param UpdateTokenModel $updateTokenModel
     */
    public function __construct(

        Service $service,
        InitFactory $initFactory,
        WriteConfig $writeConfig,
        TimezoneInterface $timezone,
        UpdateTokenModel $updateTokenModel

    ) {
        $this->service = $service;
        $this->initFactory = $initFactory;
        $this->writeConfig = $writeConfig;
        $this->timezone = $timezone;
        $this->updateTokenModel = $updateTokenModel;

        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setName(self::CONSOLE_COMMAND_NAME)
            ->setDescription('Update token');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->updateTokenModel->execute();

    }
}
