<?php
/**
 *
 * @package    Fisha
 * @category   Bit
 *  ______ _ _____ _   _
 *  |  ___(_)  ___| | | |
 *  | |_   _\ `--.| |_| | __ _
 *  |  _| | |`--. \  _  |/ _` |
 *  | |   | /\__/ / | | | (_| |
 *  \_|   |_\____/\_| |_/\__,_|
 */

namespace Fisha\Bit\Block\Adminhtml;

use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Order;

/**
 * Class Info
 * @package Fisha\Bit\Block\Adminhtml
 */
class Info extends \Magento\Payment\Block\Info
{
    /**
     * @var Order
     */
    private $order;


    /**
     * Info constructor.
     * @param Context $context
     * @param Order $order
     * @param array $data
     */
    public function __construct(
        Context $context,
        Order $order,
        array $data = []
    ) {
        $this->order = $order;

        parent::__construct($context, $data);
    }

    public function getBitPayment()
    {
        $order_id = $this->getRequest()->getParams()['order_id'];
        $order = $this->order->load($order_id);

        return $order->getPayment();
    }
}
