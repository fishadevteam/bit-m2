<?php

namespace Fisha\Bit\Model;

use Fisha\Bit\Model\Request\Init;
use Magento\Framework\HTTP\ZendClientFactory;

/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @Suppres sWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Service
{
    /**
     * @var ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     * Service constructor.
     * @param ZendClientFactory $httpClientFactory
     */
    public function __construct(
        ZendClientFactory $httpClientFactory
    ) {
        $this->httpClientFactory = $httpClientFactory;
    }

    /**
     * @param array $requestBase
     * @param $uri
     * @param array $postParams
     * @param $headers
     * @param $type
     * @param string $method
     * @return bool|string
     */
    public function postRequest(array $requestBase, $uri, array $postParams, $headers, $type, $method = 'POST')
    {
        $this->prepareCertificate($requestBase);
        $url = $requestBase['ApimHost'] . $uri;
        if ($type =='json') {
            $postParams = json_encode($postParams);
        } else {
            $postParams = http_build_query($postParams);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSLCERTTYPE, 'PEM');
        curl_setopt($curl, CURLOPT_SSLCERT, $requestBase[Init::CERT_DIR] . $requestBase[Init::CERT_PATH]);
        curl_setopt($curl, CURLOPT_SSLKEY, $requestBase[Init::CERT_DIR] . $requestBase[Init::PK_FILE]);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postParams);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        //actual curl execution perfom
        $response = curl_exec($curl);
        $error = curl_error($curl);

        if (!empty($error)) {
            //TODO: Add info to logs
            $info = curl_errno($curl) > 0 ? [curl_errno($curl) => curl_error($curl)] : curl_getinfo($curl);
            return $error;
        }
        curl_close($curl);
        return $response;
    }

    /**
     * @param $requestBase
     */
    private function prepareCertificate($requestBase)
    {
        $certDir = $requestBase[Init::CERT_DIR];
        $certPath = $requestBase[Init::CERT_PATH];

        if (!file_exists($certDir . $certPath)) {
            $data = file_get_contents($certDir . $requestBase[Init::CERT_FILE_NAME]);
            $certPassword = $requestBase[Init::CERT_PASSWD];
            $res = [];
            $openSSL = openssl_pkcs12_read($data, $res, $certPassword);
            if (!$openSSL) {
                throw new Exception("Error: " . openssl_error_string());
            }
            // this is the CER FILE
            file_put_contents($certDir . $requestBase[Init::PK_FILE], $res['pkey'] . $res['cert'] . implode('', $res['extracerts']));
            // this is the PEM FILE
            $cert = $res['cert'] . implode('', $res['extracerts']);
            file_put_contents($certDir . $requestBase[Init::CERT_PATH], $cert);
        }
    }
}
