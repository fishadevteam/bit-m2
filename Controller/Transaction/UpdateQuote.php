<?php

namespace Fisha\Bit\Controller\Transaction;

use Fisha\Bit\Controller\Transaction;
use Fisha\Bit\Logger\DebugLogger;
use Fisha\Bit\Logger\ErrorLogger;
use Fisha\Bit\Model\Payment\Bit;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class UpdateQuote
 * @package Fisha\Bit\Controller\Transaction
 */
class UpdateQuote extends Transaction
{
    /**
     * @var Session
     */
    protected $checkoutSession;


    /**
     * UpdateQuote constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Bit $bit
     * @param DebugLogger $debugLogger
     * @param ErrorLogger $errorLogger
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Bit $bit,
        DebugLogger $debugLogger,
        ErrorLogger $errorLogger,
        Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;

        parent::__construct(
            $context,
            $resultJsonFactory,
            $bit,
            $debugLogger,
            $errorLogger
        );
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        try {
            if ($this->getRequest()->isAjax()) {
                if (isset($_REQUEST['paymentInitiationId'])) {
                    $quote = $this->checkoutSession->getQuote();
                    $quote->setData('bit_payment_initiation_id', $_REQUEST['paymentInitiationId']);

                    if (!$quote->getCustomerEmail()) {
                        $quote->setCustomerEmail($quote->getBillingAddress()->getEmail());
                    }
                    if ($quote->getCustomerFirstname() === null && $quote->getCustomerLastname() === null) {
                        $quote->setCustomerFirstname($quote->getBillingAddress()->getFirstname());
                        $quote->setCustomerLastname($quote->getBillingAddress()->getLastname());
                        if ($quote->getBillingAddress()->getMiddlename() === null) {
                            $quote->setCustomerMiddlename($quote->getBillingAddress()->getMiddlename());
                        }
                    }
                    $quote->save();

                    $this->debugLogger->debug(
                        'Quote update'
                        . PHP_EOL
                        . print_r([
                            'quote_id' => $quote->getId(),
                            'bit_payment_initiation_id' => $_REQUEST['paymentInitiationId']
                        ], true)
                    );

                    $result->setData(json_encode(1));
                    return $result;
                }
            }
        } catch (\Exception $e) {
            $this->debugLogger->debug(
                'Exception during quote update'
                . PHP_EOL
                . print_r([
                    'quote_id' => isset($quote) ? $quote->getId() : null,
                    'exception' => $e->getMessage()
                ], true)
            );
        }
        $result->setData(json_encode(0));
        return $result;
    }
}
