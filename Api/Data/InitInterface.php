<?php

namespace Fisha\Bit\Api\Data;

interface InitInterface
{
    const APIM_HOST = 'apim_host';
    const SUBSCRIPTION_KEY = 'subscriptionKey';
    const BITCOM_APP_ID = 'client_id';
    const BITCOM_SECRET = 'client_secret';
    const CERT_DIR = 'cert_dir';
    const PK_FILE = 'pk_file';
    const CERT_PATH = 'cert_path';
    const CERT_FILE_NAME = 'cert_file_name';
    const CERT_PASSWD = 'cert_passwd';


    /**
     * Get apim_host
     * @return string|null
     */
    public function getApimHost();

    /**
     * Set apimHost
     * @param string $apimHost
     * @return InitInterface
     */
    public function setApimHost($apimHost);

    /**
     * Get subscriptionKey
     * @return string|null
     */
    public function getSubscriptionKey();

    /**
     * Set subscriptionKey
     * @param $subscriptionKey
     * @return InitInterface
     */
    public function setSubscriptionKey($subscriptionKey);

    /**
     * Get bitcomAppId
     * @return string|null
     */
    public function getBitcomAppId();

    /**
     * Set bitcomAppId
     * @param string $bitcomAppId
     * @return InitInterface
     */
    public function setBitcomAppId($bitcomAppId);

    /**
     * Get bitcomSecret
     * @return string|null
     */
    public function getBitcomSecret();

    /**
     * Set bitcomSecret
     * @param string $bitcomSecret
     * @return InitInterface
     */
    public function setBitcomSecret($bitcomSecret);

    /**
     * Get cert_passwd
     * @return string|null
     */
    public function getCertPasswd();

    /**
     * Set cert_passwd
     * @param string $certPasswd
     * @return InitInterface
     */
    public function setCertPasswd($certPasswd);

    /**
     * Get cert_file_name
     * @return string|null
     */
    public function getCertFileName();

    /**
     * Set cert_file_name
     * @param string $certFileName
     * @return InitInterface
     */
    public function setCertFileName(string $certFileName);

    /**
     * Get cert_path
     * @return string|null
     */
    public function getCertPath();

    /**
     * Set cert_path
     * @param string $certPath
     * @return InitInterface
     */
    public function setCertPath($certPath);

    /**
     * Get pk_file
     * @return string|null
     */
    public function getPkFile();

    /**
     * Set pk_file
     * @param string $pkFile
     * @return InitInterface
     */
    public function setPkFile($pkFile);

    /**
     * Get cert_dir
     * @return string|null
     */
    public function getCertDir();

    /**
     * Set cert_dir
     * @param string $certDir
     * @return InitInterface
     */
    public function setCertDir($certDir);
}
