define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'bit',
                component: 'Fisha_Bit/js/view/payment/method-renderer/bit-method'
            }
        );
        return Component.extend({});
    }
);