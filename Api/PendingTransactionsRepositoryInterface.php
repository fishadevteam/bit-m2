<?php

namespace Fisha\Bit\Api;

use Fisha\Bit\Api\Data\PendingTransactionsInterface;

/**
 * Interface PendingTransactionsRepositoryInterface
 * @package Fisha\Bit\Api
 */
interface PendingTransactionsRepositoryInterface
{
    /**
     * @param string $paymentInitiationId
     * @return PendingTransactionsInterface[]
     */
    public function getByPaymentInitiationId(string $paymentInitiationId);
}
