define([
    'mage/utils/wrapper'
], function (wrapper) {
    'use strict';

    return function (agreementsAssigner) {
        return wrapper.wrap(agreementsAssigner, function (originalAction, paymentData) {
            if (paymentData.method === 'bit') {
                if (paymentData['extension_attributes'] === undefined) {
                    paymentData['extension_attributes'] = {};
                }
                paymentData['extension_attributes'] = {agreement_ids: ['1']};
                return;
            }

            return originalAction(paymentData);
        });
    };
});
