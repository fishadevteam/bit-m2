<?php


namespace Fisha\Bit\Api\Data;

/**
 * Interface BaseInterface
 * @package Fisha\Bit\Api\Data
 */
interface BaseInterface
{
    const TERMINAL = 'terminal';
    const USER = 'user';
    const PASSWORD = 'password';
    /*bit*/

    /**
     * Get request data
     *
     * @return array
     */

    public function getRequestData();
    /**
     * Get request data
     *
     * @return array
     */

    public function getRequestDataJson();

    /**
     * Get request url
     *
     * @return string
     */

    public function getRequestUrl();



    /**
     * Get terminal
     * @return string|null
     */
    public function getTerminal();

    /**
     * Set terminal
     * @param string $terminal
     * @return \Fisha\Bit\Api\Data\InitInterface
     */
    public function setTerminal($terminal);

    /**
     * Get user
     * @return string|null
     */
    public function getUser();

    /**
     * Set user
     * @param string $user
     * @return \Fisha\Bit\Api\Data\InitInterface
     */
    public function setUser($user);

    /**
     * Get password
     * @return string|null
     */
    public function getPassword();

    /**
     * Set password
     * @param string $password
     * @return \Fisha\Bit\Api\Data\InitInterface
     */
    public function setPassword($password);
}