# Fisha Bit Extension 

## Installation
### Git
```bash
composer config repositories.fisha-bit-m2 vcs git@bitbucket.org:fishadevteam/bit-m2.git
composer require fishadevteam/bit-m2:1.0.6
```

### Re-build magento project
```bash
bin/magento setup:upgrade
bin/magento setup:di:compile
bin/magento setup:static-content:deploy
```
