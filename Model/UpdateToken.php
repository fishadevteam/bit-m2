<?php
namespace Fisha\Bit\Model;

use Fisha\Bit\Logger\DebugLogger;
use Fisha\Bit\Logger\ErrorLogger;
use Fisha\Bit\Model\Request\Init;
use Fisha\Bit\Model\Request\InitFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface as WriteConfig;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class UpdateToken
{
    /**
     * @var InitFactory
     */
    private $initFactory;

    /**
     * @var Service
     */
    private $service;

    /**
     * @var WriteConfig
     */
    private $writeConfig;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var DebugLogger
     */
    protected $debugLogger;

    /**
     * @var ErrorLogger
     */
    protected $errorLogger;

    /**
     * UpdateToken constructor.
     * @param Service $service
     * @param InitFactory $initFactory
     * @param WriteConfig $writeConfig
     * @param TimezoneInterface $timezone
     * @param DebugLogger $debugLogger
     * @param ErrorLogger $errorLogger
     */
    public function __construct(
        Service $service,
        InitFactory $initFactory,
        WriteConfig $writeConfig,
        TimezoneInterface $timezone,
        DebugLogger $debugLogger,
        ErrorLogger $errorLogger
    ) {
        $this->service = $service;
        $this->initFactory = $initFactory;
        $this->writeConfig = $writeConfig;
        $this->timezone = $timezone;
        $this->debugLogger = $debugLogger;
        $this->errorLogger = $errorLogger;
    }

    public function execute()
    {
        $init = $this->initFactory->create();
        $data = $init->getRequestData();
        $postParams = [
            'response_type' => 'token',
            'scope' => 'bit_payment',
            Init::BITCOM_APP_ID => $data[Init::BITCOM_APP_ID],
            Init::BITCOM_SECRET => $data[Init::BITCOM_SECRET]
        ];
        $headers = [
            'Content-Type: application/x-www-form-urlencoded',
            'Ocp-Apim-Subscription-Key: ' . $data['subscriptionKey']
        ];
        $result = $this->service->postRequest($data, Config::BIT_API_TOKEN_URL, $postParams, $headers, 'x-www');
        $result_array = json_decode($result, true);

        if (isset($result_array['access_token']) && $result_array['access_token']) {
            $this->writeConfig->save(
                Config::BIT_API_TOKEN_PATH,
                $result_array['access_token'],
                $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                $scopeId = 0
            );

            $expireDate = $this->timezone->date()->modify('+55 min')->format('Y-m-d H:i:s');
            $this->writeConfig->save(
                Config::BIT_API_TOKEN_EXPIRE_PATH,
                $expireDate,
                $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                $scopeId = 0
            );
            $this->debugLogger->debug('The API token has been updated');
        } else {
            $this->errorLogger->error('The API token was not returned from ' . Config::BIT_API_TOKEN_URL);
        }
    }
}
