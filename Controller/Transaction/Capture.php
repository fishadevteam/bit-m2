<?php

namespace Fisha\Bit\Controller\Transaction;

use Fisha\Bit\Controller\Transaction;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Capture
 * @package Fisha\Bit\Controller\Transaction
 */
class Capture extends Transaction
{
    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            if (isset($_REQUEST['data'])) {
                $data = (array)json_decode($_REQUEST['data']);
                $response = $this->bit->captureTransaction($data);
                $result->setData($response);
            }

            return $result;
        }
    }
}
