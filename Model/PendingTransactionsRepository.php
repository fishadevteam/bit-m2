<?php

namespace Fisha\Bit\Model;

use Fisha\Bit\Api\Data\PendingTransactionsInterfaceFactory;
use Fisha\Bit\Api\Data\PendingTransactionsSearchResultInterfaceFactory;
use Fisha\Bit\Api\PendingTransactionsRepositoryInterface;
use Fisha\Bit\Model\ResourceModel\PendingTransactions\CollectionFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class PendingTransactionsRepository
 * @package Fisha\Bit\Model
 */
class PendingTransactionsRepository implements PendingTransactionsRepositoryInterface
{
    /**
     * @var PendingTransactionsRepositoryInterface
     */
    protected $pendingTransactionsFactory;

    /**
     * @var PendingTransactionsSearchResultInterfaceFactory
     */
    protected $pendingTransactionsSearchResultInterface;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;


    /**
     * PendingTransactionsRepository constructor.
     * @param PendingTransactionsInterfaceFactory $pendingTransactionsFactory
     */
    public function __construct(
        PendingTransactionsInterfaceFactory $pendingTransactionsFactory,
        PendingTransactionsSearchResultInterfaceFactory $pendingTransactionsSearchResultInterface,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->pendingTransactionsFactory = $pendingTransactionsFactory;
        $this->pendingTransactionsSearchResultInterface = $pendingTransactionsSearchResultInterface;
        $this->collectionFactory = $collectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * {@inheritDoc}
     */
    public function getByPaymentInitiationId(string $paymentInitiationId)
    {
        $searchResult = $this->pendingTransactionsSearchResultInterface->create();

        if ($paymentInitiationId) {
            $searchCriteria = $this->searchCriteriaBuilder->addFilter(
                'payment_initiation_id',
                $paymentInitiationId,
                'eq'
            )->create();

            $searchResult->setSearchCriteria($searchCriteria);
            $collection = $this->collectionFactory->create();

            foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
                foreach ($filterGroup->getFilters() as $filter) {
                    $condition = $filter->getConditionType() ?: 'eq';
                    $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
                }
            }
            $searchResult->setItems($collection->getItems());
        }

        return $searchResult;
    }
}
