<?php

namespace Fisha\Bit\Model\ResourceModel\PendingTransactions;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Fisha\Bit\Model\ResourceModel\PendingTransactions
 */
class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(
            "Fisha\Bit\Model\PendingTransactions",
            "Fisha\Bit\Model\ResourceModel\PendingTransactions"
        );
    }
}
