<?php

namespace Fisha\Bit\Controller\Transaction;

use Magento\Checkout\Controller\Onepage;
use Magento\Checkout\Model\Session as checkoutSession;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Registry;
use Magento\Framework\Translate\InlineInterface;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\PaymentExtensionFactory;
use Magento\Quote\Model\Quote;

/**
 * Class ReturnUrl
 * @package Fisha\Bit\Controller\Bit
 */
class ReturnUrl extends Onepage
{
    /**
     * @var Quote
     */
    protected $quote;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @var checkoutSession
     */
    protected $checkoutSession;

    /**
     * @var PaymentExtensionFactory
     */
    protected $paymentExtensionFactory;

    /**
     * ReturnUrl constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     * @param Registry $coreRegistry
     * @param InlineInterface $translateInline
     * @param Validator $formKeyValidator
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param CartRepositoryInterface $quoteRepository
     * @param PageFactory $resultPageFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param RawFactory $resultRawFactory
     * @param JsonFactory $resultJsonFactory
     * @param Quote $quote
     * @param Customer $customer
     * @param checkoutSession $checkoutSession
     * @param PaymentExtensionFactory $paymentExtensionFactory
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        Registry $coreRegistry,
        InlineInterface $translateInline,
        Validator $formKeyValidator,
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        CartRepositoryInterface $quoteRepository,
        PageFactory $resultPageFactory,
        LayoutFactory $resultLayoutFactory,
        RawFactory $resultRawFactory,
        JsonFactory $resultJsonFactory,
        Quote $quote,
        Customer $customer,
        checkoutSession $checkoutSession,
        PaymentExtensionFactory $paymentExtensionFactory
    ) {
        $this->quote = $quote;
        $this->customer = $customer;
        $this->checkoutSession = $checkoutSession;
        $this->paymentExtensionFactory = $paymentExtensionFactory;

        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement,
            $coreRegistry,
            $translateInline,
            $formKeyValidator,
            $scopeConfig,
            $layoutFactory,
            $quoteRepository,
            $resultPageFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $resultJsonFactory
        );
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $quote = $this->quote;
        $customer = $this->customer;
        $quoteId = $this->getRequest()->getParam('quoteid');

        $quote->load($quoteId);
        $paymentMethod = $quote->getPayment();
        $paymentMethod->setExtensionAttributes($this->paymentExtensionFactory->create()->setAgreementIds([1]));

        $this->getOnepage()->setQuote($quote);
        $customer->load($quote->getCustomerId());
        $this->_customerSession->setCustomer($customer);
        $this->checkoutSession->resetCheckout();
        $this->checkoutSession->replaceQuote($quote);

        $this->_redirect('checkout', ['_fragment' => 'payment']);
    }
}
