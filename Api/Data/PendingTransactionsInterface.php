<?php

namespace Fisha\Bit\Api\Data;

/**
 * Interface PendingTransactionInterface
 * @package Fisha\Bit\Api\Data
 */
interface PendingTransactionsInterface
{
    /**
     * @return $this
     * @throws \Exception
     */
    public function save();

    /**
     * @param integer $modelId
     * @param null|string $field
     * @return $this
     */
    public function load($modelId, $field = null);
}
