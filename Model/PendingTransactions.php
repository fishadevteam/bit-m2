<?php

namespace Fisha\Bit\Model;

use Fisha\Bit\Api\Data\PendingTransactionsInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class PendingTransactions
 * @package Fisha\Bit\Model
 */
class PendingTransactions extends AbstractModel implements PendingTransactionsInterface
{
    public function _construct()
    {
        $this->_init(ResourceModel\PendingTransactions::class);
    }
}
