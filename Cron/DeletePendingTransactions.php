<?php
namespace Fisha\Bit\Cron;

use DateTime;
use Fisha\Bit\Logger\DebugLogger;
use Fisha\Bit\Logger\ErrorLogger;
use Fisha\Bit\Model\Config;
use Fisha\Bit\Model\Request\InitFactory;
use Fisha\Bit\Model\ResourceModel\PendingTransactions\CollectionFactory;
use Fisha\Bit\Model\Service;
use Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory as ConfigCollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class UpdateToken
 * @package Fisha\Bit\Cron
 */
class DeletePendingTransactions
{
    /**
     * @var DebugLogger
     */
    private $debugLogger;

    /**
     * @var ErrorLogger
     */
    private $errorLogger;

    /**
     * @var Service
     */
    private $service;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var CollectionFactory
     */
    private $pendingTransactionsFactory;

    /**
     * @var InitFactory
     */
    private $initFactory;

    /**
     * @var string
     */
    private $token;

    /**
     * @var ConfigCollectionFactory
     */
    private $configFactory;


    /**
     * DeletePendingTransactions constructor.
     * @param DebugLogger $debugLogger
     * @param ErrorLogger $errorLogger
     * @param Service $service
     * @param TimezoneInterface $timezone
     * @param CollectionFactory $pendingTransactionsFactory
     * @param InitFactory $initFactory
     * @param ConfigCollectionFactory $configFactory
     */
    public function __construct(
        DebugLogger $debugLogger,
        ErrorLogger $errorLogger,
        Service $service,
        TimezoneInterface $timezone,
        CollectionFactory $pendingTransactionsFactory,
        InitFactory $initFactory,
        ConfigCollectionFactory $configFactory
    ) {
        $this->debugLogger = $debugLogger;
        $this->errorLogger = $errorLogger;
        $this->service = $service;
        $this->timezone = $timezone;
        $this->pendingTransactionsFactory = $pendingTransactionsFactory;
        $this->initFactory = $initFactory;
        $this->configFactory = $configFactory;
    }

    /**
     * @return void
     */
    public function execute()
    {
        try {
            $currentDate = new DateTime($this->timezone->date()->format('Y-m-d H:i:s'));
            $collection = $this->pendingTransactionsFactory->create();
            foreach ($collection->getItems() as $item) {
                $expirationDate = new DateTime($item->getData('expiration_date'));
                if ($expirationDate < $currentDate && !$item->getCaptured()) {
                    $this->deleteTransaction($item->getData('payment_initiation_id'));
                    $item->delete();
                }
            }
        } catch (\Exception $e) {
            $this->errorLogger->error($e->getMessage());
        }
    }

    /**
     * @param $paymentInitiationId
     */
    private function deleteTransaction($paymentInitiationId)
    {
        $requestData = $this->initFactory->create()->getRequestData();
        $headers = [
            'Content-Type: application/json;charset=UTF-8',
            'Ocp-Apim-Subscription-Key: ' . $requestData['subscriptionKey'],
            'Authorization: Bearer ' . $this->getToken()
        ];

        $response = $this->service->postRequest(
            $requestData,
            "/payments/bit/v2/single-payments/{$paymentInitiationId}",
            [],
            $headers,
            'json',
            'DELETE'
        );

        if (isset(json_decode($response)->requestStatusCode)) {
            $this->debugLogger->debug("Deleted transaction {$paymentInitiationId}");
            $this->debugLogger->debug($response . PHP_EOL);
        } else {
            $this->errorLogger->error("Transaction {$paymentInitiationId} hasn't been deleted");
            $this->errorLogger->error($response . PHP_EOL);
        }
    }

    /**
     * @return string
     */
    private function getToken()
    {
        if (!$this->token) {
            $collection = $this->configFactory->create()
                ->addFieldToFilter('scope', ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
                ->addFieldToFilter('scope_id', 0)
                ->addFieldToFilter('path', Config::BIT_API_TOKEN_PATH)
                ->setPageSize(1);

            if ($collection->getSize()) {
                $this->token = $collection->getFirstItem()->getValue();
            } else {
                $this->errorLogger->error('Bit API token is missing');
            }
        }

        return $this->token;
    }
}
