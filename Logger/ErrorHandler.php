<?php
namespace Fisha\Bit\Logger;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;

/**
 * Class ErrorHandler
 * @package Fisha\Bit\Logger
 */
class ErrorHandler extends Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::ERROR;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/bit/error.log';
}
