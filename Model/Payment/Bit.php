<?php

namespace Fisha\Bit\Model\Payment;

use DateTime;
use Detection\MobileDetect;
use Fisha\Bit\Logger\DebugLogger;
use Fisha\Bit\Logger\ErrorLogger;
use Fisha\Bit\Model\Config as BitConfig;
use Fisha\Bit\Model\PendingTransactionsFactory;
use Fisha\Bit\Model\PendingTransactionsRepository;
use Fisha\Bit\Model\Request\InitFactory;
use Fisha\Bit\Model\Service;
use Magento\Checkout\Model\Session;
use Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory as ConfigCollectionFactory;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\UrlInterface;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Payment\Model\Method\Logger;
use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;

/**
 * Class Bit
 * @package Fisha\Bit\Model\Payment
 */
class Bit extends AbstractMethod
{
    /**
     * @var string
     */
    protected $_code = "bit";

    /**
     * @inheritdoc
     */
    protected $_isGateway = true;

    /**
     * @inheritdoc
     */
    protected $_canCapture = true;

    /**
     * @inheritdoc
     */
    protected $_canCapturePartial = true;

    /**
     * @inheritdoc
     */
    protected $_canRefund = true;

    /**
     * @inheritdoc
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * @inheritdoc
     */
    protected $_isInitializeNeeded = false;

    /**
     * @inheritdoc
     */
    protected $_canAuthorize = true;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var DebugLogger
     */
    protected $debugLogger;

    /**
     * @var ErrorLogger
     */
    protected $errorLogger;

    /**
     * @var BitConfig
     */
    protected $bitConfig;

    /**
     * @var Service
     */
    protected $service;

    /**
     * @var InitFactory
     */
    protected $initFactory;

    /**
     * @var PendingTransactionsFactory
     */
    protected $pendingTransactions;

    /**
     * @var ConfigCollectionFactory
     */
    protected $configFactory;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * @var MobileDetect
     */
    protected $detect;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var PendingTransactionsRepository
     */
    protected $pendingTransactionsRepository;


    /**
     * Bit constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param DebugLogger $debugLogger
     * @param ErrorLogger $errorLogger
     * @param BitConfig $bitConfig
     * @param Service $service
     * @param InitFactory $initFactory
     * @param PendingTransactionsFactory $pendingTransactions
     * @param PendingTransactionsRepository $pendingTransactionsRepository
     * @param ConfigCollectionFactory $configFactory
     * @param Session $checkoutSession
     * @param UrlInterface $url
     * @param MobileDetect $detect
     * @param TimezoneInterface $timezone
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     * @param DirectoryHelper|null $directory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        DebugLogger $debugLogger,
        ErrorLogger $errorLogger,
        BitConfig $bitConfig,
        Service $service,
        InitFactory $initFactory,
        PendingTransactionsFactory $pendingTransactions,
        PendingTransactionsRepository $pendingTransactionsRepository,
        ConfigCollectionFactory $configFactory,
        Session $checkoutSession,
        UrlInterface $url,
        MobileDetect $detect,
        TimezoneInterface $timezone,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [],
        DirectoryHelper $directory = null
    ) {
        $this->debugLogger = $debugLogger;
        $this->errorLogger = $errorLogger;
        $this->bitConfig = $bitConfig;
        $this->service = $service;
        $this->initFactory = $initFactory;
        $this->pendingTransactions = $pendingTransactions;
        $this->pendingTransactionsRepository = $pendingTransactionsRepository;
        $this->configFactory = $configFactory;
        $this->checkoutSession = $checkoutSession;
        $this->urlInterface = $url;
        $this->detect = $detect;
        $this->timezone = $timezone;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data,
            $directory
        );
    }

    /**
     * @inheritdoc
     */
    public function getConfigPaymentAction()
    {
        return $this->bitConfig->getTransactionType();
    }

    /**
     * @inheritdoc
     */
    public function authorize(InfoInterface $payment, $amount)
    {
        if (!$this->canAuthorize()) {
            throw new LocalizedException(__('The authorize action is not available.'));
        }
        return $this;
    }

    /**
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this|Bit
     * @throws LocalizedException
     * @throws \Exception
     */
    public function capture(InfoInterface $payment, $amount)
    {
        if (!$this->isTokenValid()) {
            $this->errorLogger->error('The Bit token is not valid');
            return $this;
        }

        if (!$this->canCapture()) {
            throw new LocalizedException(__('The capture action is not available.'));
        }

        if (!$paymentInitiationId = $payment->getAdditionalInformation('paymentInitiationId')) {
            throw new LocalizedException(__('paymentInitiationId is missing'));
        }

        try {
            $quote = $this->checkoutSession->getQuote();
        } catch (\Exception $e) {
            $this->errorLogger->error($e->getMessage(), $e->getTrace());
            return $this;
        }

        if ($this->getConfigPaymentAction() == MethodInterface::ACTION_AUTHORIZE) {
            $this->addTransactionInfo($payment);

            try {
                $response = $this->sendCaptureRequest($amount, $payment->getAdditionalInformation());
            } catch (\Exception $e) {
                $this->errorLogger->error($e->getMessage(), $e->getTrace());
                return $this;
            }

            $responseArray = json_decode($response, true);
            if (isset($responseArray['messageException']) || isset($responseArray['incidentId'])) {
                $this->errorLogger->error(print_r($responseArray, true));
                throw new \Exception(print_r($responseArray, true));
            }

            $payment->setAdditionalInformation(json_decode($response, true));
            $payment->setTransactionId($paymentInitiationId)->setIsTransactionClosed(true);
            $this->updateTransactionRecord($paymentInitiationId, false);
        }

        $requestData = $this->initFactory->create()->getRequestData();
        $firstItemName= $quote->getId();
        $itemsCollection = $quote->getItemsCollection();
        if($itemsCollection->getSize()){
            $firstItem = $itemsCollection->getFirstItem();
            $firstItemName = substr($firstItem->getName(), 0, 45);
        }
        $headers = $this->getHeaders($requestData);
        $postValues = [
            'currencyTypeCode' => BitConfig::CURRENCY_COD_TYPE,
            'debitMethodCode' => BitConfig::DEBIT_METHOD_CODE,
            'externalSystemReference' => $quote->getId() . '-' . rand(1, 10000),
            'franchisingId' => $this->bitConfig->getBitcomFranchisingId(),
            'requestAmount' => $quote->getGrandTotal(),
            'requestSubjectDescription' => "{$firstItemName}"
        ];


        $this->debugLogger->debug(
            'Transaction initiation request'
            . PHP_EOL
            . print_r($postValues, true)
        );

        $response = $this->service->postRequest(
            $requestData,
            '/payments/bit/v2/single-payments',
            $postValues,
            $headers,
            'json'
        );

        $this->debugLogger->debug(
            'Transaction initiation response'
            . PHP_EOL
            . print_r(json_decode($response), true)
        );

        return $response;
    }

    /**
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this|Bit
     * @throws LocalizedException
     */
    public function refund(InfoInterface $payment, $amount)
    {
        if (!$this->isTokenValid()) {
            // TODO: error message
            $this->errorLogger->error('The Bit token is not valid');
        }

        if (!$this->canRefund()) {
            throw new LocalizedException(__('The refund action is not available.'));
        }

        $requestData = $this->initFactory->create()->getRequestData();
        $headers = $this->getHeaders($requestData);

        $postValues = [
            'creditAmount' => $amount,
            'currencyTypeCode' => BitConfig::CURRENCY_COD_TYPE,
            'externalSystemReference' => $payment->getAdditionalInformation('externalSystemReference'),
            'paymentInitiationId' => $payment->getAdditionalInformation('paymentInitiationId'),
            'refundExternalSystemReference' => $payment->getAdditionalInformation('externalSystemReference') . '-refund'
        ];

        $response = $this->service->postRequest(
            $requestData,
            "/payments/bit/v2/refund",
            $postValues,
            $headers,
            'json',
            'POST'
        );

        $this->debugLogger->debug(
            'Refund request'
            . PHP_EOL
            . print_r($postValues, true)
        );
        $this->debugLogger->debug(
            'Refund response'
            . PHP_EOL
            . print_r(json_decode($response), true)
        );

        $response = (array)json_decode($response);
        if (isset($response['messageException'])) {
            throw new LocalizedException(__($response['messageException']));
        }
        if (isset($response['message'])) {
            throw new LocalizedException(__($response['message']));
        }
        if (isset($response['incidentId'])) {
            throw new LocalizedException(__($response['incidentId']));
        }

        return $this;
    }

    /**
     * @return bool|string
     */
    public function initTransaction()
    {
        if (!$this->isTokenValid()) {
            // TODO: error message
            $this->errorLogger->error('The Bit token is not valid');
        }

        try {
            $quote = $this->checkoutSession->getQuote();
        } catch (\Exception $e) {
            $this->errorLogger->error($e->getMessage() . PHP_EOL);
            return '';
        }

        $requestData = $this->initFactory->create()->getRequestData();
        $headers = $this->getHeaders($requestData);
        $postValues = [
            'currencyTypeCode' => BitConfig::CURRENCY_COD_TYPE,
            'debitMethodCode' => BitConfig::DEBIT_METHOD_CODE,
            'externalSystemReference' => $quote->getId() . '-' . rand(1, 10000),
            'franchisingId' => $this->bitConfig->getBitcomFranchisingId(),
            'requestAmount' => $quote->getGrandTotal(),
            'requestSubjectDescription' => "{$quote->getId()}"
        ];

        $env = $this->getEnvironment();
        if ($env == 'ios' || $env == 'android') {
            $postValues['urlReturnAddress'] = $this->urlInterface->getUrl("bit/transaction/returnurl/quoteid/{$quote->getId()}");
        }

        $this->debugLogger->debug(
            'Transaction initiation request'
            . PHP_EOL
            . print_r($postValues, true)
        );

        $response = $this->service->postRequest(
            $requestData,
            '/payments/bit/v2/single-payments',
            $postValues,
            $headers,
            'json'
        );

        $responseArray = json_decode($response, true);
        if (array_key_exists('paymentInitiationId', $responseArray)) {
            $this->addTransactionRecord($quote, $responseArray['paymentInitiationId']);
        } else {
            $this->debugLogger->debug(
                'Exception during a transaction initiation'
                . PHP_EOL
                . print_r([
                    'quote_id' => $quote->getId(),
                    'exception' => print_r($responseArray, true)
                ], true)
            );
        }

        $this->debugLogger->debug(
            'Transaction initiation response'
            . PHP_EOL
            . print_r(json_decode($response), true)
        );

        return $response;
    }

    /**
     * @param $paymentInitiationId
     * @return string
     */
    public function getTransactionStatus($paymentInitiationId)
    {
        if (!$this->isTokenValid()) {
            // TODO: error message
            $this->errorLogger->error('The Bit token is not valid');
        }

        $requestData = $this->initFactory->create()->getRequestData();
        $headers = $this->getHeaders($requestData);

        $this->debugLogger->debug(
            'Get payment status request'
            . PHP_EOL
            . print_r(['paymentInitiationId' => $paymentInitiationId], true)
        );

        $response = $this->service->postRequest(
            $requestData,
            "/payments/bit/v2/single-payments/{$paymentInitiationId}",
            [],
            $headers,
            'json',
            'GET'
        );

        $this->debugLogger->debug(
            'Get payment status response'
            . PHP_EOL
            . print_r(json_decode($response), true)
        );

        return $response;
    }

    /**
     * @param array $data
     * @return false|string
     * @throws \Exception
     */
    public function captureTransaction(array $data)
    {
        try {
            $quote = $this->checkoutSession->getQuote();

            if ($this->getConfigPaymentAction() == MethodInterface::ACTION_AUTHORIZE_CAPTURE) {
                $response = $this->sendCaptureRequest($quote->getGrandTotal(), $data);
                $responseArray = json_decode($response, true);
                if (isset($responseArray['messageException']) || isset($responseArray['incidentId'])) {
                    $this->errorLogger->error(print_r($responseArray, true));
                    throw new \Exception(print_r($responseArray, true));
                } else {
                    $this->updateTransactionRecord($data['paymentInitiationId'], true);
                }
                $quote->getPayment()->setAdditionalInformation(json_decode($response, true));
            } else {
                $this->updateTransactionRecord($data['paymentInitiationId'], true);
            }

            $quote->getPayment()->setAdditionalInformation('paymentInitiationId', $data['paymentInitiationId']);
            $quote->getPayment()->setAdditionalInformation('externalSystemReference', $data['externalSystemReference']);
            $quote->getPayment()->setAdditionalInformation('currencyTypeCode', $data['currencyTypeCode']);
            $quote->getPayment()->setAdditionalInformation('sourceTransactionId', $data['sourceTransactionId']);
            $quote->getPayment()->setAdditionalInformation('IssuerTransactionId', $data['IssuerTransactionId'] ?? '');
            $quote->save();

            $this->debugLogger->debug(
                'Quote update in capture'
                . PHP_EOL
                . print_r([
                    'quote_id' => $quote->getId(),
                    'paymentInitiationId' => $data['paymentInitiationId'],
                    'externalSystemReference' => $data['externalSystemReference']
                ], true)
            );
        } catch (\Exception $e) {
            $this->debugLogger->debug(
                'Exception during quote update in capture'
                . PHP_EOL
                . print_r([
                    'quote_id' => $quote->getId(),
                    'exception' => $e->getMessage()
                ], true)
            );

            $this->errorLogger->error($e->getMessage());
        }

        if ($this->getConfigPaymentAction() == MethodInterface::ACTION_AUTHORIZE) {
            return json_encode(['placeOrder' => 1]);
        }

        return $response;
    }

    /**
     * @param $amount
     * @param $data
     * @return bool|string
     */
    protected function sendCaptureRequest($amount, $data)
    {
        if (!$this->isTokenValid()) {
            $this->errorLogger->error('The Bit token is not valid');
        }

        $requestData = $this->initFactory->create()->getRequestData();
        $headers = $this->getHeaders($requestData);
        $postValues = [
            'requestAmount' => $amount,
            'currencyTypeCode' => $data['currencyTypeCode'],
            'externalSystemReference' => $data['externalSystemReference'],
            'paymentInitiationId' => $data['paymentInitiationId'],
            'sourceTransactionId' => $data['sourceTransactionId'],
            'IssuerTransactionId' => $data['IssuerTransactionId'] ?? ''
        ];

        $this->debugLogger->debug(
            'Capture transaction request'
            . PHP_EOL
            . print_r($postValues, true)
        );

        $response = $this->service->postRequest(
            $requestData,
            "/payments/bit/v2/single-payments/{$data['paymentInitiationId']}/capture",
            $postValues,
            $headers,
            'json',
            'POST'
        );

        $this->debugLogger->debug(
            'Capture transaction response'
            . PHP_EOL
            . print_r(json_decode($response), true)
        );

        return $response;
    }

    /**
     * @return string
     */
    public function getEnvironment()
    {
        if ($this->detect->isMobile()) {
            if ($this->detect->isiOS()) {
                $result = 'ios';
            } elseif ($this->detect->isAndroidOS()) {
                $result = 'android';
            } else {
                $result = 'mobile';
            }
        } else {
            $result = 'desktop';
        }

        return $result;
    }

    /**
     * @param Quote $quote
     * @param $paymentInitiationId
     */
    protected function addTransactionRecord(Quote $quote, $paymentInitiationId)
    {
        try {
            $currentDate = new DateTime($this->timezone->date()->format('Y-m-d H:i:s'));
            $currentDate = $currentDate->modify('+15 min');

            $transaction = $this->pendingTransactions->create();
            $transaction->addData([
                'quote_id' => $quote->getId(),
                'payment_initiation_id' => $paymentInitiationId,
                'expiration_date' => $currentDate->format('Y-m-d H:i:s'),
                'captured' => 0
            ]);
            $transaction->save();
        } catch (\Exception $e) {
            $this->errorLogger->error($e->getMessage());
        }
    }

    /**
     * @param string $paymentInitiationId
     * @param false $captured
     */
    protected function updateTransactionRecord(string $paymentInitiationId, $captured = false)
    {
        try {
            $transactions = $this->pendingTransactionsRepository
                ->getByPaymentInitiationId($paymentInitiationId)
                ->getItems();

            foreach ($transactions as $transaction) {
                $transaction->setCaptured($captured)->save();
            }
        } catch (\Exception $e) {
            $this->errorLogger->error($e->getMessage());
        }
    }

    /**
     * @return string
     */
    protected function getToken()
    {
        if (!$this->token) {
            $collection = $this->configFactory->create()
                ->addFieldToFilter('scope', ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
                ->addFieldToFilter('scope_id', 0)
                ->addFieldToFilter('path', BitConfig::BIT_API_TOKEN_PATH)
                ->setPageSize(1);

            if ($collection->getSize()) {
                $this->token = $collection->getFirstItem()->getValue();
            } else {
                $this->errorLogger->error('Bit API token is missing');
            }
        }

        return $this->token;
    }

    /**
     * @param $requestData
     * @return string[]
     */
    protected function getHeaders($requestData)
    {
        return [
            'Content-Type: application/json;charset=UTF-8',
            'Ocp-Apim-Subscription-Key: ' . $requestData['subscriptionKey'],
            'Authorization: Bearer ' . $this->getToken()
        ];
    }

    /**
     * @return bool
     */
    protected function isTokenValid()
    {
        if (!$this->getToken()) {
            return false;
        }

        $collection = $this->configFactory->create()
            ->addFieldToFilter('scope', 'default')
            ->addFieldToFilter('scope_id', 0)
            ->addFieldToFilter('path', BitConfig::BIT_API_TOKEN_EXPIRE_PATH)
            ->setPageSize(1);

        if ($collection->getSize()) {
            $expireDate = $collection->getFirstItem()->getValue();
            $currentDate = $this->timezone->date()->format('Y-m-d H:i:s');

            try {
                if (new DateTime($expireDate) > new DateTime($currentDate)) {
                    return true;
                }
            } catch (\Exception $e) {
                $this->errorLogger->error('Bit API token is expired or invalid');
            }
        } else {
            $this->errorLogger->error(BitConfig::BIT_API_TOKEN_EXPIRE_PATH . ' is missing in the database');
        }

        return false;
    }

    /**
     * @param InfoInterface $payment
     */
    protected function addTransactionInfo(InfoInterface $payment): void
    {
        if (empty($payment->getAdditionalInformation('sourceTransactionId'))) {
            $payment->setAdditionalInformation('sourceTransactionId', $this->getSourceTransactionId());
        }
        if (empty($payment->getAdditionalInformation('IssuerTransactionId'))) {
            $payment->setAdditionalInformation('IssuerTransactionId', $this->getIssuerTransactionId());
        }
    }

    /**
     * @return int
     */
    protected function getSourceTransactionId(): int
    {
        try {
            return random_int(1000000000, 9999999999);
        } catch (\Exception $e) {
            $this->errorLogger->error($e->getMessage(), $e->getTrace());
            return rand(1000000000, 9999999999);
        }
    }

    /**
     * @return string
     */
    protected function getIssuerTransactionId(): string
    {
        return (string) rand(10, 99999999);
    }
}
