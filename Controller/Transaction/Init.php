<?php

namespace Fisha\Bit\Controller\Transaction;

use Fisha\Bit\Controller\Transaction;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Init
 * @package Fisha\Bit\Controller\Transaction
 */
class Init extends Transaction
{
    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $response = $this->bit->initTransaction();
            $result->setData($response);

            return $result;
        }
    }
}
