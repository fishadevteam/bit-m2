<?php

namespace Fisha\Bit\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface PendingTransactionsSearchResultInterface
 * @package Fisha\Bit\Api\Data
 */
interface PendingTransactionsSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return PendingTransactionsInterface[]
     */
    public function getItems();

    /**
     * @param PendingTransactionsInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
