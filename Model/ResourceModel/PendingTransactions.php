<?php

namespace Fisha\Bit\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class PendingTransactions
 * @package Fisha\Bit\Model\ResourceModel
 */
class PendingTransactions extends AbstractDb
{
    public function _construct()
    {
        $this->_init("bit_pending_transactions", "entity_id");
    }
}
