<?php

namespace Fisha\Bit\Model\Request;

use Fisha\Bit\Model\Config;
use Magento\Framework\ObjectManagerInterface;

/**
 * Factory class for @see \Fisha\Bit\Model\Request\Init
 */
class InitFactory
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $instanceName;

    /**
     * @var Config
     */
    private $config;

    /**
     * InitFactory constructor
     *
     * @param Config $config
     * @param ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(
        Config $config,
        ObjectManagerInterface $objectManager,
        $instanceName = \Fisha\Bit\Model\Request\Init::class
    ) {
        $this->objectManager = $objectManager;
        $this->instanceName = $instanceName;
        $this->config = $config;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     * @return Init
     */
    public function create(array $data = [])
    {
        $data = array_merge($data, [
            Init::APIM_HOST => $this->config->getApimHost(),
            Init::SUBSCRIPTION_KEY => $this->config->getSubscriptionKey(),
            Init::BITCOM_APP_ID => $this->config->getBitcomAppId(),
            Init::BITCOM_SECRET => $this->config->getBitcomSecret(),
            Init::CERT_DIR => $this->config->getCertDir(),
            Init::PK_FILE => $this->config->getPKFile(),
            Init::CERT_PATH =>  $this->config->getCertPath(),
            Init::CERT_FILE_NAME =>  $this->config->getCertFileName(),
            Init::CERT_PASSWD =>  $this->config->getCertPasswd()

        ]);
        /** @var Init $object */
        $object = $this->objectManager->create($this->instanceName);
        return $object->setData($data);
    }
}
