<?php

namespace Fisha\Bit\Model\Request;

use Fisha\Bit\Api\Data\BaseInterface;
use Magento\Framework\DataObject;

/**
 * Class Base
 * @package Fisha\Bit\Model\Request
 */
class Base extends DataObject implements BaseInterface
{
    /**
     * Request Url
     *
     * @var string|null
     */
    protected $requestUrl;

    /**
     * Mapped request fields magentoKey => apiKey
     * @var array
     */
    protected $baseMapFields = [
        self::TERMINAL => 'terminal',
        self::USER => 'user',
        self::PASSWORD => 'password'
    ];

    /**
     * Get request data
     *
     * @return array
     */
    public function getRequestData()
    {
        $result = [];
        $data = $this->getData();
        foreach ($this->baseMapFields as $key => $apiKey) {
            if (isset($data[$key])) {
                $result[$apiKey] = $data[$key];
            }
        }
        return $result;
    }

    /**
     * @return array|false|string
     */
    public function getRequestDataJson()
    {
        $data = $this->getRequestData();
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $data[$key] = $value;
            }

        }
        return json_encode($data);
    }

    /**
     * Get request data
     *
     * @return string
     */
    public function getRequestUrl()
    {
        return $this->requestUrl;
    }

    /**
     * Get terminal
     * @return string|null
     */
    public function getTerminal()
    {
        return $this->getData(self::TERMINAL);
    }

    /**
     * Set terminal
     * @param string $terminal
     * @return $this
     */
    public function setTerminal($terminal)
    {
        return $this->setData(self::TERMINAL, $terminal);
    }

    /**
     * Get user
     * @return string|null
     */
    public function getUser()
    {
        return $this->getData(self::USER);
    }

    /**
     * Set user
     * @param string $user
     * @return $this
     */
    public function setUser($user)
    {
        return $this->setData(self::USER, $user);
    }

    /**
     * Get password
     * @return string|null
     */
    public function getPassword()
    {
        return $this->getData(self::PASSWORD);
    }

    /**
     * Set password
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        return $this->setData(self::PASSWORD, $password);
    }
}
