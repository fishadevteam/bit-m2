<?php

namespace Fisha\Bit\Model\Request;

use Fisha\Bit\Api\Data\InitInterface;

/**
 * Class Init
 * @package Fisha\Bit\Model\Request
 */
class Init extends Base implements InitInterface
{
    /**
     * Mapped request fields magentoKey => apiKey
     * @var array
     */
    protected $initMapFields = [
        self::APIM_HOST => 'ApimHost',
        self::SUBSCRIPTION_KEY => 'subscriptionKey',
        self::BITCOM_APP_ID => 'client_id',
        self::BITCOM_SECRET => 'client_secret',
        self::CERT_DIR => 'cert_dir',
        self::PK_FILE => 'pk_file',
        self::CERT_PATH => 'cert_path',
        self::CERT_FILE_NAME => 'cert_file_name',
        self::CERT_PASSWD => 'cert_passwd'
    ];

    /**
     * Get request data
     *
     * @return array
     */
    public function getRequestData()
    {
        $baseResult = parent::getRequestData();
        $result = [];
        $data = $this->getData();
        foreach ($this->initMapFields as $key => $apiKey) {
            if (isset($data[$key])) {
                $result[$apiKey] = $data[$key];
            }
        }
        return array_merge($baseResult, $result);
    }

    /**
     * Get cert_passwd
     * @return string|null
     */
    public function getCertPasswd()
    {
        return $this->getData(self::CERT_FILE_NAME);
    }

    /**
     * Set cert_passwd
     * @param string $certPasswd
     * @return $this
     */
    public function setCertPasswd($certPasswd)
    {
        return $this->setData(self::CERT_FILE_NAME, $certPasswd);
    }

    /**
     * Get cert_file_name
     * @return string|null
     */
    public function getCertFileName()
    {
        return $this->getData(self::CERT_FILE_NAME);
    }

    /**
     * Set cert_file_name
     * @param string $certFileName
     * @return $this
     */
    public function setCertFileName(string $certFileName)
    {
        return $this->setData(self::CERT_FILE_NAME, $certFileName);
    }

    /**
     * Get cert_path
     * @return string|null
     */
    public function getCertPath()
    {
        return $this->getData(self::CERT_PATH);
    }

    /**
     * Set cert_path
     * @param string $certPath
     * @return $this
     */
    public function setCertPath($certPath)
    {
        return $this->setData(self::CERT_PATH, $certPath);
    }

    /**
     * Get pk_file
     * @return string|null
     */
    public function getPkFile()
    {
        return $this->getData(self::PK_FILE);
    }

    /**
     * Set pk_file
     * @param string $pkFile
     * @return $this
     */
    public function setPkFile($pkFile)
    {
        return $this->setData(self::PK_FILE, $pkFile);
    }

    /**
     * Get cert_dir
     * @return string|null
     */
    public function getCertDir()
    {
        return $this->getData(self::CERT_DIR);
    }

    /**
     * Set cert_dir
     * @param string $certDir
     * @return $this
     */
    public function setCertDir($certDir)
    {
        return $this->setData(self::CERT_DIR, $certDir);
    }

    /**
     * Get apim_host
     * @return string|null
     */
    public function getApimHost()
    {
        return $this->getData(self::APIM_HOST);
    }

    /**
     * Set apim_host
     * @param string $apimHost
     * @return $this
     */
    public function setApimHost($apimHost)
    {
        return $this->setData(self::APIM_HOST, $apimHost);
    }

    /**
     * Get subscriptionKey
     * @return string|null
     */
    public function getSubscriptionKey()
    {
        return $this->getData(self::SUBSCRIPTION_KEY);
    }

    /**
     * Set subscriptionKey
     * @param string $subscriptionKey
     * @return $this
     */
    public function setSubscriptionKey($subscriptionKey)
    {
        return $this->setData(self::SUBSCRIPTION_KEY, $subscriptionKey);
    }

    /**
     * Get BitcomAppId
     * @return string|null
     */
    public function getBitcomAppId()
    {
        return $this->getData(self::BITCOM_APP_ID);
    }

    /**
     * Set BitcomAppId
     * @param string $bitcomAppId
     * @return $this
     */
    public function setBitcomAppId($bitcomAppId)
    {
        return $this->setData(self::BITCOM_APP_ID, $bitcomAppId);
    }

    /**
     * Get BitcomSecret
     * @return string|null
     */
    public function getBitcomSecret()
    {
        return $this->getData(self::BITCOM_SECRET);
    }

    /**
     * Set BitcomSecret
     * @param string $bitcomSecret
     * @return $this
     */
    public function setBitcomSecret($bitcomSecret)
    {
        return $this->setData(self::BITCOM_SECRET, $bitcomSecret);
    }
}
