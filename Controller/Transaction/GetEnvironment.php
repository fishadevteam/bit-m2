<?php

namespace Fisha\Bit\Controller\Transaction;

use Fisha\Bit\Controller\Transaction;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Get
 * @package Fisha\Bit\Controller\Token\Get
 */
class GetEnvironment extends Transaction
{
    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $result->setData($this->bit->getEnvironment());

            return $result;
        }
    }
}
